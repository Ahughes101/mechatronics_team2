#Author: Team2
#Platform Python 3.5
#front_end.py used as the front end platform to communicate with the crane across the relevant bluetooth COM port
#Before using make sure the device is paired with "T2_crane" using code: 1001
#Possible issues may occur if you do not have pyserial library installed


import serial           #both the pyserial and tkinter libraries are imported for use
import tkinter as tk

serv = serial.Serial()      #create an instance of the serial connection called serv
serv.port = 'COM5'          #bluetooth com port for the computer - This will need to be changed dependant on the device you are using
serv.baudrate = 9600        #bluetooth module baud rate
serv.open()                 #open the serial connection using the above port and baudrate

class Application(tk.Frame):        #object defined for the control panel window
    def __init__(self,root):
        super().__init__(root)      #used so you dont have to call the base class for every method
        self.root = root
        self.grid()     #used the grid command to place the frame root in the window
        self.root.title("Team2 Crane control panel")        #window title changed
        self.config(bg = "ivory4")      #background colour of control panel set
        self.set_widgets()              #both set_widgets andpack_widgets are defined below
        self.pack_widgets()
        self.dccounter = False      #status variables used to toggle the indicator labels
        self.dcStatus = False
        self.ElectroStatus = False
        self.DistStatus = False

    def set_widgets(self):  #the physical widgets are defined below using labels and buttons from the tkinter python library
        self.left = tk.Button(self,text = "<- Servo left ",font = "Tahoma 12",width = "30",height = "6",command = self.crane_left)
        self.right = tk.Button(self,text = "Servo right ->",font = "Tahoma 12",width = "30",height = "6",command = self.crane_right)
        self.electro = tk.Button(self,text = "Toggle electromagnet",font = "Tahoma 12",width = "30",height = "6",command = self.electroToggle)
        self.dc = tk.Button(self,text = "Toggle DC motor",font = "Tahoma 12",width = "30",height = "6",command = self.dcToggle)
        self.exit = tk.Button(self,text = "Quit",font = "Tahoma 12",width = "30",height = "6",command = self.shutdown)
        self.electroStatus = tk.Label(self,text = "Electromagnet Status",font = "Courier 12 bold",bg = "red",width = "25",height = "7")
        self.DCStatus = tk.Label(self,text = "DC motor Status",font = "Courier 12 bold",bg = "red",width = "25",height = "7")
        self.distStatus = tk.Label(self,text = "Object detected \nby distance sensor!",font = "Courier 12 bold",bg = "red",width = "25",height = "7")

    def pack_widgets(self):   #method to arrange the labels and buttons on the window using the grid command
        self.electro.grid(row = "2",column = "3")
        self.dc.grid(row = "2",column = "2")
        self.left.grid(row = "2",column = "1")
        self.right.grid(row = "2",column = "4")
        self.exit.grid(row = "3",column = "1")
        self.electroStatus.grid(row = "1", column = "5")
        self.DCStatus.grid(row = "2",column = "5")
        self.distStatus.grid(row = "3",column = "5")

    def crane_right(self):      #method that sends a char over the serial connection and updates the labels
        serv.write(b'd\r\n')
        self.update_status()

    def crane_left(self):       #method that sends a char over the serial connection and updates the labels
        serv.write(b'a\r\n')
        self.update_status()

    def dcToggle(self):         #bluetooth command sent to turn on or off the blueotth motor, the backend arduino code will control the motor direction
        if self.dccounter == True:
            serv.write(b'c\r\n')    #toggle between turning sending the on or off command to the arduino
        else:
            serv.write(b'z\r\n')
        self.dccounter = not self.dccounter
        self.update_status()

    def electroToggle(self):    #bluetooth command sent to toggle the electromagnet
        serv.write(b'o\r\n')
        self.update_status()

    def shutdown(self):
        self.root.destroy() #closes the window when the exit button is pressed
        serv.close()        #close the bluetooth connection once the window is closed

    def update_status(self):
        read = serv.read(3)     #read the char sent from the bluetooth module to update the status labels. char has a max length of 3 bytes
        if read == b'm\r\n':    #if m is sent via the serial connection the label is toggled from either green to red or vice versa
            if self.dcStatus == False:
                self.DCStatus.config(bg = "green")
                self.dcStatus = True
            else:
                self.DCStatus.config(bg = "red")
                self.dcStatus = False
        elif read == b'e\r\n':  #if e is sent via the serial connection the label is toggled from either green to red or vice versa
            if self.ElectroStatus == False:
                self.electroStatus.config(bg = "green")
                self.ElectroStatus = True
            else:
                self.electroStatus.config(bg = "red")
                self.ElectroStatus = False
        elif read == b'i\r\n':  #if i is sent via the serial connection the label is toggled from either green to red or vice versa
            if self.DistStatus == False:
                self.distStatus.config(bg = "green")
                self.DistStatus = True
            else:
                self.distStatus.config(bg = "red")
                self.DistStatus = False


root = tk.Tk()              #create an instance of the Tk command defined by the variable root
app = Application(root)     #create a window called app passing root as a frame
app.mainloop()              #creates a while loop that automatically updates the tkinter window
serv.close()                #once the window is closed close the bluetooth connection