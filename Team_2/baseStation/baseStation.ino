/* Author: Team2
 * File used on the basestation to light up 
 * LED's dependant on the mass placed on an FSR
 */

int LEDpin1 = 11;   //digital pins being used for the respective LED's
int LEDpin2 = 10;
int LEDpin3 = 9;
int LEDpin4 = 8;

void setup() 
{
  Serial.begin(9600);
  pinMode(LEDpin1, OUTPUT);   //set each LED to be an output
  pinMode(LEDpin2, OUTPUT);
  pinMode(LEDpin3, OUTPUT);
  pinMode(LEDpin4, OUTPUT);
}

void loop() 
{
  int fsrAnalogPin = 0;
  int fsrReading = analogRead(fsrAnalogPin);    //defined the variable tobe the analog input of the FSR
  Serial.print("Force sensor reading = ");    
  Serial.println(fsrReading);
  //output the FSR reading to the serial monitor in case the user would like to analyse this information

  digitalWrite(LEDpin1, LOW); //each LED is set to low intially
  digitalWrite(LEDpin2, LOW);
  digitalWrite(LEDpin3, LOW);
  digitalWrite(LEDpin4, LOW);
  
  if(fsrReading > 0 && fsrReading < 21) //range of the FSR sensor for masses less than 100g
  {
    digitalWrite(LEDpin1, HIGH);    //only the Acceptable LED on
    digitalWrite(LEDpin2, LOW);
    digitalWrite(LEDpin3, LOW);
    digitalWrite(LEDpin4, LOW);
  }
  if(fsrReading >21 && fsrReading<60)   //range of the FSR sensor for masses between 100g and 200g
  {
    digitalWrite(LEDpin1, HIGH);    //first two LED's on and the Medium and Heavy ones off
    digitalWrite(LEDpin2, HIGH);
    digitalWrite(LEDpin3, LOW);
    digitalWrite(LEDpin4, LOW);
  }
  if(fsrReading >60 && fsrReading <105) //range of the FSR sensor for masses between 200g and 300g
  {
    digitalWrite(LEDpin1, HIGH);    //first three LED's on and the 4th one off
    digitalWrite(LEDpin2, HIGH);
    digitalWrite(LEDpin3, HIGH);
    digitalWrite(LEDpin4, LOW);
  }
  else  //for masses larger than 400g
  {
    digitalWrite(LEDpin1, HIGH);  //all three LEDs on
    digitalWrite(LEDpin2, HIGH);
    digitalWrite(LEDpin3, HIGH);
  
    for(int i=0;i<5;i++)    //The 4th warning LED will flash for 5 seconds
    {
      digitalWrite(LEDpin4, HIGH);
      delay(500);     //half second delay between the LED beign on and off
      digitalWrite(LEDpin4, LOW);
      delay(500);
    }
  }
  delay(10);    //standard loop delay
}
