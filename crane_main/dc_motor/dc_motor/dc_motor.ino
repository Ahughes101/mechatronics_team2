//test code to test the dc motor

#include <AFMotor.h>

int ain1 = 9;    //direction
int ain2 = 8;     //direction
int pwma = 3;     //speed
int standby = 2;
int m_direction = 0;  //start with forward can be changed

static boolean motor1 = 1;

void setup() 
{
  pinMode(standby,OUTPUT);
  pinMode(ain1,OUTPUT); 
  pinMode(ain2,OUTPUT); 
  pinMode(pwma,OUTPUT); 

}

void loop()
{
  m_direction = 1;
  motorDrive(motor1,m_direction,255);   //drive motor forward full speed
  delay(5000);
  m_direction = 0;
  motorDrive(motor1,m_direction,255);   //drive motor forward full speed
  delay(5000);

}


void motorDrive(boolean motorNum, int motorDir, int motorSpeed)
{
  boolean pinIn;
  digitalWrite(standby,HIGH);
  if(motorDir == 0)
  {
    pinIn = HIGH;
  }
  else
  {
    pinIn = LOW;
  }

  digitalWrite(ain1,pinIn);
  digitalWrite(ain2,!pinIn);
  analogWrite(pwma,motorSpeed);
}

