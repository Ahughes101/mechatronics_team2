/* Author: Team2
 * Main file used to control the crane
 * Bluetooth connection with control panel in front_end.py
 */


#include <SoftwareSerial.h>
#include <Servo.h>

int ain1 = 9;    //direction pin for dc motor
int ain2 = 8;     //direction pin for dc motor
int pwma = 3;     //pwm signal pin for dc motor which controls speed
int standby = 2;  //standby pin for dc motor
int m_direction = 0;  
int Electromagnet = 7;
boolean electro_state = LOW; //start using electromagnet off
const long BTbaudRate = 9600; 
int servoPin = 11;   //change to the digital pin the servo is actually connected
int servoSpeed = 90;    //initial servo speed 90 is stop
int distPin1 = 1;   //analogue 1
int distPin2 = 2;     //analogue 2

Servo servo1;    //servo left declared
SoftwareSerial BTserial(6,5); //tx and rx port numbers used by bluetooth module

void setup() 
{
  Serial.begin(9600); 
  pinMode(Electromagnet, OUTPUT);   //setup dc motor and electromagnet pins as outputs
  pinMode(standby,OUTPUT); 
  pinMode(ain1,OUTPUT); 
  pinMode(ain2,OUTPUT); 
  pinMode(pwma,OUTPUT);
  
  servo1.attach(servoPin);    //attach the digital pin to the servo object
  servo1.write(servoSpeed);   //write intial servo speed to the servo object
  
  //bluetooth setup
  BTserial.begin(BTbaudRate);
  delay(1000);                //delay backoff time as per datasheet
  if(BTserial.available()>0)
  {
    while(BTserial.available()>0)
    {
      char c = BTserial.read();
      Serial.write(c);
    }
  }
  BTserial.print("AT+NAMET2_crane");    //bluetooth connection name
  delay(1000);                //delay backoff time for the bluetooth module
  if(BTserial.available()>0)
  {
    while(BTserial.available()>0)
    {
      char c = BTserial.read();
      Serial.write(c);
    }
  }
  BTserial.print("AT+PIN1001");   //bluetooth connection pin
  delay(1000);                 //delay backoff time for the bluetooth module
  if(BTserial.available()>0)
  {
    while(BTserial.available()>0)
    {
      char c = BTserial.read();
      Serial.write(c);
    }
  }
  delay(100);   //initial setup delay
}

void loop() 
{
  int dist1 = analogRead(distPin1);   //distance sensor pin values read every loop iteration
  int dist2 = analogRead(distPin2);
  char to_send, bt_input = ' '; //empty char declaration of bt input value
  int actionOccurred = 0;   //integer to indicate if a command has been carrried out
  bt_input = BTserial.read();   //input from keyboard
  if(bt_input == 'd')     //char indicator for right servo movement
  {
    servoSpeed = 10;    //servo speed for right movement
    to_send = 's';    //send char to bluetooth device indicating servo is moving
    actionOccurred = 1;
  }
  else if(bt_input == 'a')    //char indicator recieved to turn servo left
  {
    servoSpeed = 170;
    to_send = 's';    //send char to bluetooth device indicating servo is moving
    actionOccurred = 1;
    
  }
  else if(bt_input == 'z')  //char indicator recieved to turn on dc motor and or inverse its direction
  {
    m_direction = !m_direction;   //inverse motor direction
    drive(m_direction,100);   //drive the dc motor using function below
    to_send = 'm';    //send char to indicate dc motor is moving
    actionOccurred = 1;
  }
  else if(bt_input == 'c')    //char indicator to stop the dc motor
  {
    drive(m_direction,0); //stop motor
    to_send = 'm';    //send char to indicate the dc motor has ended operation
    actionOccurred = 1;
  }
  else if(bt_input == 'o')    //char recieved to turn on electro magnet, intial state is zero
  {
    electro_state = !electro_state;
    digitalWrite(Electromagnet, electro_state);
    to_send = 'e';
    actionOccurred = 1;
  }
  else
  {
    servoSpeed = 90;    //if no command is sent the servo will not move
    if(actionOccurred == 1) (actionOccurred = 0);   //condition reset to show an action has finished occuring
    //the above statement prevents the system continuously sending the char indicating what action has occurred
  }
  
  if(dist1<500 && dist2<500)  //check distance sensor is within acceptable limits
  {
    servo1.write(servoSpeed);   //move the servo motor at the desired speed
  }
  else    //if either distance sensor is triggered
  {
    BTserial.println('i');    //send i to python to indicate a distance sensor has been triggered
    servoSpeed = 90;    //set speed so the crane doesnt move
    servo1.write(servoSpeed);
    drive(m_direction,0);   //stop the DC motor
    delay(5000);    //5 second delay to stop the crane from injuring someone
    BTserial.println('i');    //send i to python to confirm wait is over
  }
  if(actionOccurred == 1)(BTserial.println(to_send));   //if an action has occurred send the relevantchar to python
  delay(100);
}

void drive(int motorDir, int motorSpeed)    //drive function to drive the DC motor
{
  boolean pinIn;    //boolena used to write the relevant bit to the H bridge
  digitalWrite(standby,HIGH);   //write the High to the standby pin on the H bridge 
  if(motorDir == 0)   //checks which direction to move the DC motor
  {
    pinIn = HIGH;
  }
  else
  {
    pinIn = LOW;
  }
  digitalWrite(ain1,pinIn);   //writes relevant bits to the H bridge
  digitalWrite(ain2,!pinIn);
  analogWrite(pwma,motorSpeed);   //writes the speed and PWM signal to the H bridge
}


